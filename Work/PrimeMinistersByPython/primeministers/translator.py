#! /usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import os
import re

import table
import tuple

class Translator(object):
	"""トランスレータ：総理大臣のCSVファイルをHTMLページへと変換するプログラム。"""

	def __init__(self, input_table):
		"""トランスレータのコンストラクタ。"""
		self._input_table = input_table
		self._output_table = table.Table("output")
		return

	def compute_string_of_days(self, period):
		"""在位日数を計算して、それを文字列にして応答する。"""
		searched = re.findall("[0-9]+",period)
		start_year = searched[0]
		start_month = searched[1]
		start_day = searched[2]
		start = datetime.date(int(start_year),int(start_month),int(start_day))
		
		if len(searched) > 3:
			end_year = searched[3]
			end_month = searched[4]
			end_day = searched[5]
			end = datetime.date(int(end_year),int(end_month),int(end_day))
			return_period = end - start
		else :
			return_period = datetime.date.today() - start

		return str("{0:,d}".format(int(return_period.days)+1))

	def compute_string_of_image(self, tuple):
		"""サムネイル画像から画像へ飛ぶためのHTML文字列を作成して、それを応答する。"""
		values = tuple.values()
		thumbnails_string = values[-1]
		searched = re.findall("[0-9]+[\.jpg]",thumbnails_string)
		string_html_link = "<a name=\""+values[0]+"\" href=\""+values[-2]+"\"><img class=\"borderless\" src=\""+values[-1]+"\"+width=\"25\" height=\"32\" alt="+searched[0]+".jpg\"></a>"
		return string_html_link

	def table(self):
		"""総理大臣のCSVファイルを基にしたテーブルから、HTMLページを基にするテーブルに変換して、それを応答する。"""

		tuples = self._input_table.tuples()
		
		for a_tuple in tuples:
			values = a_tuple.values()
			if values[4] != "在位期間":
				period = values[4]
				string_days = self.compute_string_of_days(period)
				string_image_html = self.compute_string_of_image(a_tuple)
				output_values = []
				output_values.append(values[0])
				output_values.append(values[1])
				output_values.append(values[2])
				output_values.append(values[3])
				output_values.append(values[4])
				output_values.append(string_days)
				output_values.append(values[5])
				output_values.append(values[6])
				output_values.append(values[7])
				output_values.append(string_image_html)
				output_tuple = tuple.Tuple(self._output_table.attributes(),output_values)
				self._output_table.add(output_tuple)
		return self._output_table
