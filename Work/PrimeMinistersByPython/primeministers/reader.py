#! /usr/bin/env python
# -*- coding: utf-8 -*-

import io
import table
import tuple

class Reader(io.IO):
	"""リーダ：総理大臣の情報を記したCSVファイルを読み込んでテーブルに仕立て上げる。"""

	def __init__(self, csv_filename):
		"""リーダのコンストラクタ。"""
		self.csv_filename = csv_filename
		return

	def table(self):
		"""ダウンロードしたCSVファイルを読み込んでテーブルを応答する。"""
		a_reader = super(Reader,self).read_csv(self.csv_filename)
		a_table = table.Table('input')
		if a_reader is None:
			print "a_reader is None"
		else:
			for row in a_reader:
				a_table.add(tuple.Tuple(a_table.attributes(),row))
		return a_table
