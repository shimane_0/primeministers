#! /usr/bin/env python
# -*- coding: utf-8 -*-

import csv

class IO(object):
	"""入出力：リーダ・ダウンローダ・ライタを抽象する。"""

	def read_csv(self, filename):
		"""指定されたファイルをCSVとして読み込む。"""
		return csv.reader(open(filename, 'rU'), delimiter=',', quotechar='"')

	def write_csv(self, filename, rows):
		"""指定されたファイルにCSVとして行たち(rows)を書き出す。"""
		csv.writer(open(filename, 'wb'), delimiter=',', quotechar='|', quoting = QUOTE_MINIMAL)
		return
