#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
import urllib

import io
import reader

class Downloader(io.IO):
	"""ダウンローダ：総理大臣のCSVファイル・画像ファイル・サムネイル画像ファイルをダウンロードする。"""

	def __init__(self, base_directory):
		"""ダウンローダのコンストラクタ。"""
		self.base_directory = base_directory
		self.url = "http://www.cc.kyoto-su.ac.jp/~atsushi/Programs/CSV2HTML/PrimeMinisters"
		self.filename = "/PrimeMinisters.csv"
		return

	def download_all(self):
		"""すべて（総理大臣の情報を記したCSVファイル・画像ファイル群・縮小画像ファイル群）をダウンロードし、テーブルを応答する。"""
		os.mkdir(self.base_directory+"/images");
		os.mkdir(self.base_directory+"/thumbnails");
		self.download_csv()
		a_reader = reader.Reader(self.base_directory+self.filename);
		a_table = a_reader.table();
		self.download_images(a_table.image_filenames())
		self.download_images(a_table.thumbnail_filenames())
		return a_table

	def download_csv(self):
		"""総理大臣の情報を記したCSVファイルをダウンロードする。"""
		urllib.urlretrieve(self.url+"/"+self.filename,self.base_directory+"/"+self.filename)
		# print "url : "+self.url
		# print "dir : "+self.base_directory
		return

	def download_images(self, image_filenames):
		"""画像ファイル群または縮小画像ファイル群をダウンロードする。"""
		for image_filename in image_filenames:
			urllib.urlretrieve(self.url+"/"+image_filename,self.base_directory+"/"+image_filename)
		return
