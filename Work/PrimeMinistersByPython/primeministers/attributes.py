#! /usr/bin/env python
# -*- coding: utf-8 -*-

class Attributes(object):
	"""属性リスト：総理大臣の情報テーブルを入出力する際の属性情報を記憶。"""

	def __init__(self, kind_string):
		"""入力用("input")または出力用("output")で属性リストを作成するコンストラクタ。"""
		if kind_string == "input":
			self._keys = ['No', 'Orders', 'Name', 'Kana', 'Period', 'School', 'Party', 'Place', 'Image', 'Thumbnails']
			self._names = ['人目','代','名前','ふりがな','在位期間','出身校','政党','出身地','画像','縮小画像']
		elif kind_string == "output":
			self._keys = ['No', 'Orders', 'Name', 'Kana', 'Period', 'Days', 'School', 'Party', 'Place', 'Image']
			self._names = ['人目','代','名前','ふりがな','在位期間','在位日数','出身校','政党','出身地','画像']
		return

	def __str__(self):
		"""自分自身を文字列にして、それを応答する。"""
		str_self = __name__
		return str_self

	def keys(self):
		"""キー群を応答する。"""
		return self._keys

	def names(self):
		"""名前群を応答する。"""
		return self._names

	def set_names(self, names):
		"""名前群を設定する。"""
		self._names = names
		return
