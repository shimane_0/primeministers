#! /usr/bin/env python
# -*- coding: utf-8 -*-

import attributes

class Table(object):
	"""表：総理大臣の情報テーブル。"""

	def __init__(self, kind_string):
		"""テーブルのコンストラクタ。"""
		self._tuples = []
		self._keys = []
		self._images = []
		self._thumbnails = []
		self._attributes = attributes.Attributes(kind_string)
		self._kind_string = kind_string
		return

	def __str__(self):
		"""自分自身を文字列にして、それを応答する。"""
		str_self = __name__
		return str_self

	def add(self, tuple):
		"""タプルを追加する。"""
		an_attributes = tuple.attributes()
		self._keys = an_attributes.keys()
		self._tuples.append(tuple)
		return

	def attributes(self):
		"""属性リストを応答する。"""
		return self._attributes

	def image_filenames(self):
		"""画像ファイル群をリストにして応答する。"""
		for tuple in self._tuples:
			values = tuple.values()
			if values[-2] != "画像" :
				self._images.append(values[-2])
		return self._images

	def thumbnail_filenames(self):
		"""縮小画像ファイル群をリストにして応答する。"""
		for tuple in self._tuples:
			values = tuple.values()
			if values[-1] != "縮小画像":
				self._thumbnails.append(values[-1])
		return self._thumbnails
	
	def tuples(self):
		"""タプル群を応答する。"""
		return self._tuples
