package primeministers;

import java.util.Calendar;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * トランスレータ：総理大臣のCSVファイルをHTMLページへと変換するプログラム。
 */
public class Translator extends Object
{
	/**
	 * CSVに由来するテーブルを記憶するフィールド。
	 */
	private Table inputTable = null;
	
	/**
	 * HTMLに由来するテーブルを記憶するフィールド。
	 */
	private Table outputTable = null;
	
	/**
	 * トランスレータのコンストラクタ。
	 */
	public Translator()
	{
		super();
		inputTable = new Table();
		return;
	}
	
	public String computeNumberOfDays(String periodString)
	{
			
			Calendar fromCalendar = Calendar.getInstance();
			
			Calendar toCalendar = Calendar.getInstance();
			
			//System.out.println(toCalendar);
			
			ArrayList<String> matchList = new ArrayList<String>();
			
			String regex = "[0-9]+";
			
			Pattern pattern = Pattern.compile(regex);
			
			Matcher matcher = pattern.matcher(periodString);
			
			while(matcher.find()==true)
			{
				matchList.add(matcher.group());
			}
			
			//System.out.println(matchList);
			
			fromCalendar.set(Integer.parseInt(matchList.get(0)),Integer.parseInt(matchList.get(1)),Integer.parseInt(matchList.get(2)));
			//System.out.println(fromCalendar);
			long from = fromCalendar.getTimeInMillis();
			//System.out.println(from);
			
			if(3 < matchList.size())
			{
				toCalendar.set(Integer.parseInt(matchList.get(3)),Integer.parseInt(matchList.get(4)),Integer.parseInt(matchList.get(5)));
				//System.out.println(toCalendar);
			}
			else
			{
				int year = toCalendar.get(Calendar.YEAR);
				int month = toCalendar.get(Calendar.MONTH)+1;
				int day = toCalendar.get(Calendar.DATE)+1;
				toCalendar.set(year,month,day);
			}
			
			long to = toCalendar.getTimeInMillis();
			//System.out.println(to);
			long template = 1000*60*60*24;
			long days = (to - from)/template;
			//System.out.println(days);
			String daysString = String.format("%1$,3d", days);
			
			return daysString;
	}
	
	public String computeStringOfImage(String aString,Tuple aTuple,int no)
	{
		int imageIndex = aTuple.attributes().indexOfImage();
		int thumbnailIndex = aTuple.attributes().indexOfThumbnail();
		ArrayList<String> values = aTuple.values();
		//総理大臣がもしNo.1000になったらバグが起きるかも・・・(何年後？)
		String noString = String.format("%1$03d", Integer.parseInt(values.get(no)));
		String image = values.get(imageIndex);
		String thumbnail = values.get(thumbnailIndex);
		aString = "<a name=\""+no+"\" href=\""+image+"\"><img class=\"borderless\" src=\""+thumbnail+"\"width=\"25\" height=\"32\" alt="+noString+".jpg\"></a>";
		return aString;
	}

	/**
	 * 総理大臣のCSVファイルをHTMLページへ変換する。
	 */
	public void perform()
	{
		
		/**ダウンローダーとライターとリーダーの完成待ち**/
		Writer aWriter = new Writer();
		Downloader aDownloader = new Downloader();
		this.inputTable = aDownloader.table();
		this.outputTable = table(this.inputTable);
		aDownloader.downloadImages();
		aDownloader.downloadThumbnails();
		Table aTable = aWriter.table(this.outputTable);
		String aString = "総理大臣のCSVファイルからHTMLページへの変換を無事に完了しました。\n";
		JOptionPane.showMessageDialog(null, aString, "報告", JOptionPane.PLAIN_MESSAGE);
		return;
	}
	
	/**
	 * 総理大臣のCSVファイルを基にしたテーブルから、HTMLページを基にするテーブルに変換して、それを応答する。
	 */
	public Table table(Table aTable)
	{
		Table returnTable = new Table();
		returnTable.attributes(new Attributes("output"));
		
		ArrayList<Tuple> tuples = aTable.tuples();
		
		Attributes inputAttributes = aTable.attributes();
		int inputNoIndex = inputAttributes.indexOfNo();
		int inputOrderIndex = inputAttributes.indexOfOrder();
		int inputNameIndex = inputAttributes.indexOfName();
		int inputKanaIndex = inputAttributes.indexOfKana();
		int inputPeriodIndex = inputAttributes.indexOfPeriod();
		int inputSchoolIndex = inputAttributes.indexOfSchool();
		int inputPartyIndex = inputAttributes.indexOfParty();
		int inputPlaceIndex = inputAttributes.indexOfPlace();
		int inputImageIndex = inputAttributes.indexOfImage();
		int inputThumbnailIndex = inputAttributes.indexOfThumbnail();
		
		Attributes returnAttributes = returnTable.attributes();
		int outputNoIndex = returnAttributes.indexOfNo();
		int outputOrderIndex = returnAttributes.indexOfOrder();
		int outputNameIndex = returnAttributes.indexOfName();
		int outputKanaIndex = returnAttributes.indexOfKana();
		int outputPeriodIndex = returnAttributes.indexOfPeriod();
		int outputDaysIndex = returnAttributes.indexOfDays();
		int outputSchoolIndex = returnAttributes.indexOfSchool();
		int outputPartyIndex = returnAttributes.indexOfParty();
		int outputPlaceIndex = returnAttributes.indexOfPlace();
		int outputImageIndex = returnAttributes.indexOfImage();
		
		for(Tuple tuple : tuples)
		{
			ArrayList<String> values = tuple.values();
			ArrayList<String> outputValues = new ArrayList<String>(values);
			outputValues.set(outputNoIndex,tuple.values().get(inputNoIndex));
			outputValues.set(outputOrderIndex,tuple.values().get(inputOrderIndex));
			outputValues.set(outputNameIndex,tuple.values().get(inputNameIndex));
			outputValues.set(outputKanaIndex,tuple.values().get(inputKanaIndex));
			outputValues.set(outputPeriodIndex,tuple.values().get(inputPeriodIndex));
			outputValues.set(outputDaysIndex,computeNumberOfDays(tuple.values().get(inputPeriodIndex)));
			outputValues.set(outputSchoolIndex,tuple.values().get(inputSchoolIndex));
			outputValues.set(outputPartyIndex,tuple.values().get(inputPartyIndex));
			outputValues.set(outputPlaceIndex,tuple.values().get(inputPlaceIndex));
			outputValues.set(outputImageIndex,computeStringOfImage("None",tuple,tuple.attributes().indexOfNo()));
			Tuple outputTuple = new Tuple(new Attributes("output"),outputValues);
			returnTable.add(outputTuple);
		}
		
		return returnTable;
	}
	
}
