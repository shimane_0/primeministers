package primeministers;

import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.lang.NullPointerException;
import java.lang.SecurityException;

/**
 * 入出力：リーダ・ダウンローダ・ライタを抽象する。
 */
public abstract class IO extends Object
{
	/**
	 * tableを束縛するフィールド
	 */
	protected Table table;
	
	/**
	 * IOのコンストラクタ
	 */
	public IO()
	{
		super();
		this.table = null;
		return;
	}
	
	/**
	 * ファイルやディレクトリを削除するクラスメソッド。
	 */
	public static void deleteFileOrDirectory(File aFile)
	{
		if(aFile.exists())
		{
			try
			{
				aFile.delete();
			}
			catch(SecurityException e)
			{
				e.printStackTrace();
			}
		}
		return;
	}
	
	/**
	 * 総理大臣ページのためのディレクトリ（存在しなければ作成して）を応答するクラスメソッド。
	 */
	public static File directoryOfPages()
	{
		File aDirectory = null;
		String dirString = System.getProperty("user.home")
						 + File.separator + "Desktop"
						 + File.separator + "PrimeMinisters";
		try
		{
			aDirectory = new File(dirString);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
		if(aDirectory.exists())
		{
			if(!aDirectory.isDirectory())
			{
				deleteFileOrDirectory(aDirectory);
				try
				{
					aDirectory.mkdir();
				}
				catch(SecurityException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			try
			{
				aDirectory.mkdir();
			}
			catch(SecurityException e)
			{
				e.printStackTrace();
			}
		}
		return aDirectory;
	}
	
	/**
	 * 入出力する際の文字コードを応答するクラスメソッド。
	 */
	public static String encodingSymbol()
	{
		return "UTF-8";
	}
	
	/**
	 * 指定されたファイルからテキストを読み込んで、それを行リストにして応答するクラスメソッド。
	 */
	public static ArrayList<String> readTextFromFile(File aFile)
	{
		ArrayList<String> lineList = new ArrayList<String>();
		BufferedReader aReader = null;
		String aString = null;
		try
		{
			aReader = new BufferedReader(new InputStreamReader(new FileInputStream(aFile), IO.encodingSymbol()));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			while((aString = aReader.readLine()) != null)
			{
				lineList.add(aString);
			}
			aReader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return lineList;
	}
	
	/**
	 * 指定されたファイル文字列からテキストを読み込んで、それを行リストにして応答するクラスメソッド。
	 */
	public static ArrayList<String> readTextFromFile(String fileString)
	{
		File aFile = null;
		try
		{
			aFile = new File(fileString);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		return IO.readTextFromFile(aFile);
	}
	
	/**
	 * 指定されたURL文字列からテキストを読み込んで、それを行リストにして応答するクラスメソッド。
	 */
	public static ArrayList<String> readTextFromURL(String urlString)
	{
		URL aURL = null;
		try
		{
			aURL = new URL(urlString);
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace();
		}
		return  IO.readTextFromURL(aURL);
	}
	
	/**
	 * 指定されたURL文字列からテキストを読み込んで、それを行リストにして応答するクラスメソッド。
	 */
	public static ArrayList<String> readTextFromURL(URL aURL)
	{
		ArrayList<String> lineList = new ArrayList<String>();
		BufferedReader aReader = null;
		String aString = null;
		try
		{
			aReader = new BufferedReader(new InputStreamReader(aURL.openStream(), IO.encodingSymbol()));
		}
		catch(IOException anIOException)
		{
			anIOException.printStackTrace();
		}
		
		try
		{
			while((aString = aReader.readLine()) != null)
			{
				lineList.add(aString);
			}
			aReader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return lineList;
	}
	
	/**
	 * 文字列をセパレータで分割したトークン列を応答するクラスメソッド。
	 */
	public static ArrayList<String> splitString(String string, String separators)
	{
		ArrayList<String> tokenList = new ArrayList<String>();
		String[] strArray = string.split(separators);
		for(String aString : strArray)
		{
			tokenList.add(aString);
		}
		return tokenList;
	}
	
	/**
	 * テーブルを応答する。
	 */
	public Table table()
	{
		return this.table;
	}
	
	/**
	 * 指定された行リストを、指定されたファイルに書き出すクラスメソッド。
	 */
	public static void writeText(ArrayList<String> aCollection, File aFile)
	{
		BufferedWriter aWriter = null;
		try
		{
			aWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(aFile), IO.encodingSymbol()));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			for(String aString : aCollection)
			{
				aWriter.write(aString);
				aWriter.write("\n");
			}
			aWriter.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return;
	}
	
	/**
	 * 指定された行リストを、指定されたファイル名のファイルに書き出すクラスメソッド。
	 */
	public static void writeText(ArrayList<String> aCollection, String fileString)
	{
		File aFile = null;
		try
		{
			aFile = new File(fileString);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		IO.writeText(aCollection, aFile);
		return;
	}
}
