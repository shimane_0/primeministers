package primeministers;

import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.lang.String;
import javax.imageio.ImageIO;
import java.io.File;
import java.lang.NullPointerException;
import java.io.IOException;

/**
 * 表：総理大臣の情報テーブル。
 */
public class Table extends Object
{
	/**
	 * 属性リストを記憶するフィールド。
	 */
	private Attributes attributes;
	
	/**
	 * タプル群を記憶するフィールド。
	 */
	private ArrayList<Tuple> tuples;
	
	/**
	 * 画像群を記憶するフィールド。
	 */
	private ArrayList<BufferedImage> images;
	
	/**
	 * サムネイル画像群を記憶するフィールド。
	 */
	private ArrayList<BufferedImage> thumbnails;
	
	/**
	 * テーブルのコンストラクタ。
	 */
	public Table()
	{
		this.tuples = new ArrayList<Tuple>();
		this.attributes = null;
		this.images = null;
		this.thumbnails = null;
		return;
	}
	
	/**
	 * タプルを追加する。
	 */
	public void add(Tuple aTuple)
	{
		this.tuples.add(aTuple);
		return;
	}
	
	/**
	 * 属性リストを応答する。
	 */
	public Attributes attributes()
	{
		return this.attributes;
	}
	
	/**
	 * 属性リストを設定する。
	 */
	public void attributes(Attributes instanceOfAttributes)
	{
		this.attributes = instanceOfAttributes;
		return;
	}
	
	/**
	 * 画像群を応答する。
	 */
	public ArrayList<BufferedImage> images()
	{
		ArrayList<String> value;
		for(Tuple aTuple: tuples)
		{
			value = aTuple.values();
			String aString = value.get(8);
			if(aString.equals("画像") == false)
			{
				this.images.add(picture(aString));
			}
		}
		return this.images;
	}
	
	/**
	 * 画像またはサムネイル画像の文字列を受けとって当該画像を応答する。
	 */
	private BufferedImage picture(String aString)
	{
		BufferedImage aBufferedImage = null;
		File imageFile = null;
		String dirString = System.getProperty("user.home")
						 + File.separator + "Desktop" 
						 + File.separator + "SouriDaijin"
						 + File.separator + "images";
		try
		{
			imageFile = new File(dirString,  aString);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
		if(imageFile.exists())
		{
			try
			{
				aBufferedImage = ImageIO.read(imageFile);
			}
			catch(NullPointerException e)
			{
				e.printStackTrace();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		return aBufferedImage;
	}
	
	/**
	 * サムネイル画像群を応答する。
	 */
	public ArrayList<BufferedImage> thumbnails()
	{
		ArrayList<String> value;
		for(Tuple aTuple: tuples)
		{
			value = aTuple.values();
			String aString = value.get(9);
			if(aString.equals("縮小画像") == false)
			{
				this.thumbnails.add(picture(aString));
			}
		}
		return this.thumbnails;
	}
	
	/**
	 * 自分自身を文字列にして、それを応答する。
	 */
	public String toString()
	{
		StringBuffer aBuffer = new StringBuffer();
		aBuffer.append(this.attributes());
		aBuffer.append("\n");
		for(Tuple aTuple: this.tuples())
		{
			aBuffer.append(aTuple);
			aBuffer.append("\n");
		}
		aBuffer.deleteCharAt(aBuffer.length() - 1);
		return aBuffer.toString();
	}
	
	/**
	 * タプル群を応答する。
	 */
	public java.util.ArrayList<Tuple> tuples()
	{
		return this.tuples;
	}
}
