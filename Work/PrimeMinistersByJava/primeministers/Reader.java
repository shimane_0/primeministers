package primeministers;

import java.io.File;
import java.util.ArrayList;
import java.lang.NullPointerException;

/**
 * リーダ：総理大臣の情報を記したCSVファイルを読み込んでテーブルに仕立て上げる。
 */
public class Reader extends IO
{
	File filename;
	
	/**
	 *Readerのコンストラクタ
	 */
	public Reader()
	{
		super();
		this.filename = filenameOfCSV();
		return;
	}
	
	/**
	 *ダウンロードしたCSVファイルを応答する。
	 */
	public File filename()
	{
		return this.filename;
	}
	
	/**
	 *ダウンロードしたCSVファイルのローカルなファイルを応答するクラスメソッド。
	 */
	public static File filenameOfCSV()
	{
		String aString = directoryOfPages().getPath();
		File aFile = null;
		try
		{
			aFile = new File(aString + File.separator + "PrimeMinisters.csv");
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		return aFile;
	}
	
	/**
	 *ダウンロードしたCSVファイルを読み込んでテーブルを応答する。
	 */
	public Table table()
	{
		//if(table != null) return table;
		ArrayList<String> lineList = readTextFromFile(filename());
		Table table = new Table();
		boolean firstAttributes = true;
		
		for(String aLine : lineList)
		{
			ArrayList<String> tokenList = splitString(aLine,",");
			if(firstAttributes)
			{
				table.attributes(new Attributes("input"));
				table.attributes().names(tokenList);
				firstAttributes = false;
			}
			else
			{
				table.add(new Tuple(table.attributes(), tokenList));
			}
		}
		return table;
	}
	
}
