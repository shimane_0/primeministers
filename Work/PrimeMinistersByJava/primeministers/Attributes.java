package primeministers;

import java.util.ArrayList;

/**
 * 属性群：総理大臣の情報テーブルを入出力する際の属性情報を記憶。
 */
public class Attributes extends Object
{
	
	/**
	 *属性リストのキー群を記憶するフィールド。
	 */
	private ArrayList<String> keys;
	
	/**
	 *属性リストの名前群を記憶するフィールド。
	 */
	private ArrayList<String> names;
	
	/**
	 *入力用("input")または出力用("output")で属性リストを作成するコンストラクタ。
	 */
	public Attributes(String aString)
	{
		this.keys = new ArrayList<String>();
		this.names = new ArrayList<String>();
		if (aString.compareTo("input") == 0)
		{
			String[] keyStrings = new String[]
			{
				"No","Order","Name","Kana","Period","School","Party","Place","Image","Thumbnails"
			};
			
			String[] nameStrings = new String[]
			{
				"人目","代","名前","ふりがな","在位期間","出身校","政党","出身地","画像","縮小画像"
			};
			
			for(String key : keyStrings)
			{
				this.keys.add(key);
			}
			for(String name : nameStrings)
			{
				this.names.add(name);
			}
		}
		else if (aString.compareTo("output") == 0)
		{
			String[] keyStrings = new String[]
			{
				"No","Order","Name","Kana","Period","Days","School","Party","Place","Image"
			};
			
			String[] nameStrings = new String[]
			{
				"人目","代","名前","ふりがな","在位期間","在日日数","出身校","政党","出身地","画像"
			};
			
			for(String key : keyStrings)
			{
				this.keys.add(key);
			}
			for(String name : nameStrings)
			{
				this.names.add(name);
			}
		}
			
	}
	
	/**
	 *指定されたインデックスに対応する名前を応答する。名前が無いときはキーを応答する。
	 */
	protected String at(int index)
	{
		
		return this.names.get(index);
	}
	
	/**
	 *指定されたキー文字列のインデックスを応答する。
	 */
	private int indexOf(String aString)
	{
		return this.keys.indexOf(aString);
	}
	
	/**
	 *在位日数のインデックスを応答する。
	 */
	public int indexOfDays()
	{
		return this.keys.indexOf("Days");
	}
	
	
	/**
	 * 画像のインデックスを応答する。
	 */
	public int indexOfImage()
	{
		return this.keys.indexOf("Image");
	}
	
	/**
	 * ふりがなのインデックスを応答する。
	 */
	public int indexOfKana()
	{
		return this.keys.indexOf("Kana");
	}
	
	/**
	 * 氏名のインデックスを応答する。
	 */
	public int indexOfName()
	{
		return this.keys.indexOf("Name");
	}
	
	/**
	 * 番号のインデックスを応答する。
	 */
	public int indexOfNo()
	{
		return this.keys.indexOf("No");
	}
	
	/**
	 * 代のインデックスを応答する。
	 */
	public int indexOfOrder()
	{
		return this.keys.indexOf("Order");
	}
	
	/**
	 * 政党のインデックスを応答する。
	 */
	public int indexOfParty()
	{
		return this.keys.indexOf("Party");
	}
	
	/**
	 * 在位期間のインデックスを応答する。
	 */
	public int indexOfPeriod()
	{
		return this.keys.indexOf("Period");
	}
	
	/**
	 * 出身地のインデックスを応答する。
	 */
	public int indexOfPlace()
	{
		return this.keys.indexOf("Place");
	}
	
	/**
	 * 出身校のインデックスを応答する。
	 */
	public int indexOfSchool()
	{
		return this.keys.indexOf("School");
	}
	
	/**
	 * 画像のインデックスを応答する。
	 */
	public int indexOfThumbnail()
	{
		return this.keys.indexOf("Thumbnails");
	}
	
	/**
	 * 指定されたインデックスに対応するキーを応答する。
	 */
	protected String keyAt(int index)
	{
		return this.keys.get(index);
	}
	
	/**
	 * キー群を応答する。
	 */
	public ArrayList<String> keys()
	{
		return keys;
	}
	
	/**
	 * 指定されたインデックスに対応する名前を応答する。
	 */
	protected String nameAt(int index)
	{
		return this.names.get(index);
	}
	
	/**
	 * 名前群を応答する。
	 */
	public ArrayList<String> names()
	{
		return this.names;
	}
	
	/**
	 * 名前群を設定する。
	 */
	public void names(ArrayList<String> aCollection)
	{
		this.names = aCollection;
		return;
	}
	
	/**
	 * 属性リストの長さを応答する。
	 */
	public int size()
	{
		return this.keys.size();
	}
	
	/**
	 *自分自身を文字列にして、それを応答する。
	 */
	public String toString()
	{
		StringBuffer stringBuffer = new StringBuffer();
        Class aClass = getClass();
        stringBuffer.append(aClass.getName());
        return stringBuffer.toString();
	}
	
}
