package primeministers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;

/**
 * ダウンローダ：総理大臣のCSVファイル・画像ファイル・サムネイル画像ファイルをダウンロードする。
 */
public class Downloader extends IO
{

	private String url = null;
	
	/**
	 *ダウンローダのコンストラクタ。
	 */
	public Downloader()
	{
		super();
		url = urlStringOfCSV();
		
	}
	
	/**
	 *総理大臣の情報を記したCSVファイルをダウンロードする。
	 */
	public void downloadCSV()
	{
		ArrayList<String> aCollection = IO.readTextFromURL(this.url);
		File aFile = Reader.filenameOfCSV();
		IO.writeText(aCollection, aFile);
		return;
	}
	
	/**
	 *総理大臣の画像群をダウンロードする。
	 */
	public void downloadImages()
	{
		File aFile = new File(super.directoryOfPages(),"images/");
		aFile.mkdir();
		int index = this.table.attributes().indexOfImage();
		this.downloadPictures(index);
		return;
	}
	
	/**
	 *総理大臣の画像群またはサムネイル画像群をダウンロードする。
	 */
	private void downloadPictures(int indexOfPicture)
	{
		File aDirectory = IO.directoryOfPages();
		for(Tuple aTuple : this.table().tuples())
		{
			URL aURL = null;
			BufferedImage anImage = null;
			String aString = aTuple.values().get(indexOfPicture);
			
			try
			{
				aURL = new URL(this.urlString()+aString);
				//System.out.println("aURL:"+aURL);
			}
			catch(MalformedURLException e)
			{
				e.printStackTrace();
			}
			
			try
			{
				anImage = ImageIO.read(aURL);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			
			try
			{
				ImageIO.write(anImage, "jpg", new File(aDirectory.getPath() + File.separator + aString));
			}
			catch (IOException anException)
			{
				anException.printStackTrace();
			}
			
		}
		return;
	}
	
	/**
	 *総理大臣の画像群をダウンロードする。
	 */
	public void downloadThumbnails()
	{
		File aFile = new File(super.directoryOfPages(),"thumbnails/");
		aFile.mkdir();
		int index = this.table.attributes().indexOfThumbnail();
		//System.out.println("index : "+this.table.attributes().indexOfThumbnail());
		this.downloadPictures(index);
		return;
	}
	
	/**
	 *総理大臣の情報を記したCSVファイルをダウンロードして、画像群やサムネイル画像群をダウロードし、テーブルで応答する。
	 */
	public Table table()
	{
		this.downloadCSV();
		Reader aReader = new Reader();
		this.table = aReader.table();
		return this.table;
	}
	
	/**
	 *総理大臣の情報を記したCSVファイルの在処(URL)を文字列で応答する。
	 */
	public String url()
	{		
		return this.url;
	}
	
	/**
	 *総理大臣の情報の在処(URL)を文字列で応答するクラスメソッド。
	 */
	public static String urlString()
	{
		return "http://www.cc.kyoto-su.ac.jp/~atsushi/Programs/CSV2HTML/PrimeMinisters/";
	}
	
	/**
	 *総理大臣の情報を記したCSVファイルの在処(URL)を文字列で応答するクラスメソッド。
	 */
	public static String urlStringOfCSV()
	{
		return Downloader.urlString()+"PrimeMinisters.csv";
	}
	
}
